import dropbox
import xml.etree.ElementTree as ET
import os

# Dropbox OAuth2 Token
DROPBOX_TOKEN = os.environ.get('DROPBOX_TOKEN')
DROPBOX_MAPS_PATH = os.environ.get('DROPBOX_MAPS_PATH', '/TrackMania Maps/For Server')
TRACKMANIA_MAPS_PATH = os.environ.get('TRACKMANIA_MAPS_PATH', 'downloads/')
TRACKMANIA_MAPS_FILE = os.environ.get('TRACKMANIA_MAPS_FILE', './test.xml')

def get_files():
    db = dropbox.Dropbox(DROPBOX_TOKEN)

    list_folder_result = db.files_list_folder(DROPBOX_MAPS_PATH)
    maps = []
    for entry in list_folder_result.entries:
        print('Downloading ' + entry.name)
        map_path = TRACKMANIA_MAPS_PATH + entry.name
        db.files_download_to_file(map_path, entry.path_lower)
        maps.append(map_path)
    return maps

def parse_xml(maps):
    tree = ET.parse(TRACKMANIA_MAPS_FILE)
    root = tree.getroot()
    for challenge in root.findall('challenge'):
        root.remove(challenge)
    for map_path in maps:
        challenge = ET.Element('challenge')
        file_element = ET.SubElement(challenge, 'file')
        file_element.text = map_path
        root.append(challenge)
    tree.write('output.xml')

def main():
    if DROPBOX_TOKEN is None:
        print('Environment variable DROPBOX_TOKEN is required')
        return

    maps = get_files()
    parse_xml(maps)

if __name__ == '__main__':
    main()